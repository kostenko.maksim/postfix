# Postfix yandex relay



## Как сконфигурировать postfix, чтобы он отправлял почту через SMTP-сервер Яндекса

Имеем:
- делегированный домен на Яндекс @custom-domain.ru
- созданный ящик info@custom-domain.ru
- не забываем залогиниться один раз и согласиться с условиями
- активируем в настройках: Все настройки → Почтовые программы → IMAP → Портальный пароль

## Переходим к настройкам postfix:

Дописываем в основной конфиг /etc/postfix/main.cf следующие строки:

```shell
smtp_sasl_auth_enable = yes
smtp_tls_security_level = encrypt
smtp_tls_wrappermode = yes
smtp_sasl_security_options = noanonymous

smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd

relayhost = [smtp.yandex.ru]:465
```

Создаем файл с логином-паролем
```shell
$ sudo touch /etc/postfix/sasl_passwd
$ sudo chmod 0640 /etc/postfix/sasl_passwd
```

И пишем туда:
```shell
[smtp.yandex.ru]:465  info@custom-domain.ru:my-password
```

Генерируем файл .bd для postfix:
```shell
$ sudo postmap /etc/postfix/sasl_passwd
```

Перечитываем конфигурацию postfix:
```shell
$ postifx reload
postfix/postfix-script: refreshing the Postfix mail system

```

## Делаем тестовую отправку:
Смотрим логи
```shell
$ sudo tail -f /var/log/mail.log
```
А из другого выполняем отправку почты адресату test@custom-domain.ru:
```shell
$ echo "Test text" | mail -s "Test title" test@custom-domain.ru
```
Получим ошибку:

```shell
553 5.7.1 Sender address rejected: not owned by auth user... (in reply to MAIL FROM command))
```
В поле FROM должен быть установлен отправитель из файла sasl_passwd, то есть info@custom-domain.ru

## Донастраиваем Postfix

Дописываем в основной конфиг /etc/postfix/main.cf следующие строки:

```shell
smtp_generic_maps = hash:/etc/postfix/generic
```
Создаем файл:
```shell
$ sudo touch /etc/postfix/generic
$ sudo chmod 0640 /etc/postfix/generic
```

И пишем туда:
```shell
@s23  info@custom-domain.ru
```

Примечение: s23 обычно взят из параметра в /etc/postfix/main.cf, все зависит от настроек postfix:
```shell
myhostname = s23
```
Генерируем файл .bd для postfix:
```shell
$ sudo postmap /etc/postfix/generic
```
Перечитываем конфигурацию postfix:
```shell
$ postifx reload
postfix/postfix-script: refreshing the Postfix mail system

```

Выполняем отправку почты адресату test@custom-domain.ru:
```shell
$ echo "Test text" | mail -s "Test title" test@custom-domain.ru
```
Успех. Ошибок нет, письмо пришло.

Тест отправки
```shell
$ echo "Test text" | mail -s "Test title" test@custom-domain.ru -aFrom:other@custom-domain.ru
```